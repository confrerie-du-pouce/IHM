import Vue from 'vue'
import Router from 'vue-router'
import App from './App.vue'
import HelloWorld from "./components/welcome";
import franceMap from "./components/franceMap";
import infoTGV from "./components/infoTGV";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false;

Vue.use(Router);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

const routes = [
  {path : '/', component: HelloWorld, name: 'accueil'},
  {path: '/francemap', component: franceMap, name:'map'},
  {path: '/infotgv', component: infoTGV, name:'tgv'}
];

const router = new Router({routes});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
