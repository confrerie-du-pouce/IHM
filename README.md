# SNCF Dataviz

Ce projet a pour but de créer une interface pour visualiser un type de données fournie par la SNCF.

## Fonctionnalité

TER:
- Affichage d'une carte des regions cliquable colorée en fonction du pourcentage de regularité des TER et du mois sélectionné.
- Affichage de différentes statistiques lors du choix d'une region.

TGV:
- Affichage d'un graphe de la régularité des TGV entre deux gares sélectionnées sur une durée choisie jusqu'une date définie par l'utilisateur.

## Installation et lancement

- Ce projet nécessite node.js
- Dans le dossier src lancer la commande `npm Install` pour installer tous les modules nécessaires.
- Dans le même dossier lancer la commande `npm run serve` pour lancer le serveur.
- Le site sera donc accessible à l'adresse http://localhost:8080/

## Outils
- [Chart.js](https://www.chartjs.org)
- [Vue.js](https://vuejs.org)
- [API SNCF](https://data.sncf.com/explore/?sort=modified)
- [Figma](https://www.figma.com/files/project/19588908/SNCF-Dataviz)
